import { Route, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { LayoutComponent } from '../layout/layout.component';
import { NgModule } from '@angular/core';


export const homeRoutes: Route[] = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'home',
        component: HomeComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(homeRoutes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
